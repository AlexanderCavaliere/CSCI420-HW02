# HW02_CAVALIERE_Alexander_program.py
# Alexander R. Cavaliere <arc6393@rit.edu>
# 2017-02-10

# Allows us to read/write csv files
import csv
# Used for mean() function (I'm lazy :shrug:)
import statistics
# Used for binning the data for the histogram
import numpy
# Used for generating the histogram and plots
import matplotlib.pyplot as plt

# Contains the logic to perform OTSU's method, read in csv files,
# and create histogram plots
class Highway_Speeder_Classification:

    # Reads in the csv file
    def Read_Data( filename ):
        data = []
        # Read in the CSV file 
        with open( filename, newline='' ) as csvfile:
            reader = csv.reader( csvfile, dialect='excel', delimiter=' ', quotechar='\'' )
            # Go row by row and store each value in our data list
            for row in reader:
               for element in row:
                   data.append( float(element) ) # Save it as a float to retain accuracy
        return data # Return the list so we use it later

    # Create the histogram of just the 128 vehicles' speeds
    def Make_Histogram_Initial( data, filename ):
        histogram = plt.hist( data, \
                              bins = numpy.arange( float(round(min(data))) + 4.0,\
                                                   float(round(max(data))) - 2.0,\
                                                   2.0 ) ); # Make the histogram with the speeds and bins
        # Add the labels to the figure
        plt.xlabel( "Vehicle Speed (MPH)" )
        plt.ylabel( "Number of Cars" )
        plt.title( "Histogram of Speeds (2 MPH Per Bin)" )
        # Save the figure
        plt.savefig( filename ) # name of the file.png
        return histogram # Return the histogram for processing 

    # OTSU's method implementation
    def Find_Best_Mixed_Variance( histogram, data_points ):
        best_variance = float('inf') # Set the initial best variances to infinity
        best_threshold = -1 # Set the speed threshold to a negative value
        variances = [] # Empty list to store the variances
        for threshold in histogram[1]: # Loop through each bin's speed 
            speeds_above = [] # List to store variances below threshold
            speeds_below = [] # List to store speeds below threshold
            for speed in data_points: # Loop over each of the collected speeds
                if threshold >= speed: # If our threshold is greater than the speed
                    speeds_below.append( speed ) # Place it within the below list
                elif threshold < speed: # If our threshold is less than or equal to the speed
                    speeds_above.append( speed ) # Place it within the above list

            if speeds_above == [] or speeds_below == []:
                continue # If a list is empty we won't be able to calculate the variances so keep moving
            # Find the fraction of speeds above the threshold
            weight_above = len( speeds_above ) / ( len( speeds_above ) + len( speeds_below ) )
            # Find the fraction of speeds at or below the treshold
            weight_below = len( speeds_below ) / ( len( speeds_above ) + len( speeds_below ) )
            variance_above = 0 # The variance of two equal points is 0 (so if we have one point the variance ~== 0)
            variance_below = 0
            if len( speeds_above ) > 1: # Check to make sure we have at least 2 points
                variance_above = statistics.variance( speeds_above )
            if len( speeds_below ) > 1:
                variance_below = statistics.variance( speeds_below )
            # Calculate the weighted variance using the below formula
            weighted_variance = ( weight_above * variance_above ) + ( weight_below * variance_below )
            # Add the variance to the list
            variances.append( weighted_variance )
            # Check to see if we have found a better variance
            if weighted_variance <= best_variance:
                best_variance = weighted_variance # Should we find a better variance; replace the old one with it
                best_threshold = threshold # Record the threshold this variance occurred at 
        # Print out our findings
        print( best_variance ) 
        print( best_threshold )
        # Return the generated data and the bin speeds
        return ( best_threshold, best_variance, variances, histogram[1] )
        
    # Plot the Bin Speeds compared to the Weighted Variances
    def Make_Variance_Speed_Plot( data, filename ):
        best_variance = data[0]
        best_threshold = data[1]
        variances = data[2]
        speeds = data[3]
        # Clear the original Plot and Plot the Variance function
        plt.clf()
        plt.plot( speeds, variances )
        # Add the labels
        plt.xlabel( "Vehicle Speeds (MPH)" )
        plt.ylabel( "Weighted Variance of Speed" )
        plt.title( "Weighted Variances of each Threshold" )
        # Save the figure
        plt.savefig( filename ) # Name of file.png
        plt.clf() # Clear the figure after we save it so we're ready to go with the next figure!

    # Creates a histogram of the mystery data and saves it as fig (We've done all the
    # work to generate it, why not plot it?)
    def Make_Histogram_Initial_Mystery( data, filename ):
        # Create the number of bins based on the data
        bins = numpy.linspace( min(data), max(data), (max(data) - min(data))/2 )
        # Plot the histogram
        histogram = plt.hist( data, bins )
        # Label the plot
        plt.xlabel( "Mystery Data Values (Some Unit?)" )
        plt.ylabel( "Mystery Data Values in Each Bin ( (max - min) / 2 bins )" )
        # Give the plot a title
        plt.title( "Mystery Data Histogram" )
        # Save the plot to a file
        plt.savefig( filename )
        return histogram  

# Instantiate the class to perform OTSU's method
HighwaySpeeder = Highway_Speeder_Classification

# Deal with the speed data
data = HighwaySpeeder.Read_Data( "UNCLASSIFIED_Speed_Observations_for_128_vehicles.csv" ) # Read in the data
# Plot the initial histogram of speeds in bins of size 2
histogram = HighwaySpeeder.Make_Histogram_Initial( data, "Speeder_classification.png" )
# Perform OTSU's method on the speed histogram
binarized_data = HighwaySpeeder.Find_Best_Mixed_Variance( histogram, data )
# Make the best threshold and lowest variance on histogram
HighwaySpeeder.Make_Variance_Speed_Plot( binarized_data, "Speed_Variance_Plot.png" )

# Deal with the Mystery Data
mystery_data = HighwaySpeeder.Read_Data( "Mystery_Data.csv" ) # Read in the data
# Make the first histogram
mystery_histogram = HighwaySpeeder.Make_Histogram_Initial_Mystery( mystery_data, "Mystery_Classification.png" )
# Find the best threshold and lowest variance
binarized_mystery_data = HighwaySpeeder.Find_Best_Mixed_Variance( mystery_histogram, mystery_data )

